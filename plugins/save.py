# Ultroid - UserBot
# Copyright (C) 2021 TeamUltroid
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.
"""
✘ Commands Available -

• `{i}save <reply message>`
    Save that replied msg to ur saved messages box.

• `{i}fsave <reply message>`
    Forward that replied msg to ur saved messages.

•`{i}megadl <link>`
  It Downloads and Upload Files from mega.nz links.

• `{i}pntrst <link/id>`
    Download and send pinterest pins.

• `{i}logo <text>`
   Generate a logo of the given Text
   Or Reply To image , to write ur text on it.
   Or Reply To Font File, To write with that font.
"""
import time
from datetime import datetime
import os
from urllib.request import urlretrieve as donl
import glob
import random

from PIL import Image, ImageDraw, ImageFont
from telethon.tl.types import InputMessagesFilterPhotos

from bs4 import BeautifulSoup as bs
from requests import get

from . import *

@ultroid_cmd(pattern="save$")
async def saf(e):
    x = await e.get_reply_message()
    if not x:
        return await eod(
            e, "Reply to Any Message to save it to ur saved messages", time=5
        )
    if e.out:
        await e.client.send_message("me", x)
    else:
        await e.client.send_message(e.sender_id, x)
    await eod(e, "Message saved to Your Pm/Saved Messages.", time=5)


@ultroid_cmd(pattern="fsave$")
async def saf(e):
    x = await e.get_reply_message()
    if not x:
        return await eod(
            e, "Reply to Any Message to save it to ur saved messages", time=5
        )
    if e.out:
        await x.forward_to("me")
    else:
        await x.forward_to(e.sender_id)
    await eod(e, "Message saved to Your Pm/Saved Messages.", time=5)

@ultroid_cmd(pattern="megadl ?(.*)")
async def _(e):
    link = e.pattern_match.group(1)
    if not os.path.isdir("mega"):
        os.mkdir("mega")
    else:
        os.system("rm -rf mega")
        os.mkdir("mega")
    xx = await eor(e, f"Processing...\nTo Check Progress : `{HNDLR}ls mega`")
    s = datetime.now()
    x, y = await bash(f"megadl {link} --path mega")
    ok = get_all_files("mega")
    tt = time.time()
    c = 0
    for kk in ok:
        try:
            res = await uploader(kk, kk, tt, xx, "Uploading...")
            await e.client.send_file(
                e.chat_id,
                res,
                caption="`" + kk.split("/")[-1] + "`",
                force_document=True,
                thumb="resources/extras/ultroid.jpg",
            )
            c += 1
        except Exception as er:
            LOGS.info(er)
    ee = datetime.now()
    t = time_formatter(((ee - s).seconds) * 1000)
    size = 0
    for path, dirs, files in os.walk("mega"):
        for f in files:
            fp = os.path.join(path, f)
            size += os.path.getsize(fp)
    await xx.delete()
    await e.client.send_message(
        e.chat_id,
        f"Downloaded And Uploaded Total - `{c}` files of `{humanbytes(size)}` in `{t}`",
    )
    os.system("rm -rf mega")
_base = "https://pinterestdownloader.com/download?url="


def gib_link(link):
    colon = "%3A"
    slash = "%2F"
    if link.startswith("https"):
        return _base + link.replace(":", colon).replace("/", slash)
    else:
        return _base + f"https{colon}{slash}{slash}pin.it{slash}{link}"


@ultroid_cmd(
    pattern="pntrst ?(.*)",
)
async def pinterest(e):
    m = e.pattern_match.group(1)
    get_link = get(gib_link(m)).text
    hehe = bs(get_link, "html.parser")
    hulu = hehe.find_all("a", {"class": "download_button"})
    if len(hulu) < 1:
        return await eod(e, "`Wrong link or private pin.`")
    elif len(hulu) > 1:
        donl(hulu[0]["href"], "pinterest.mp4")
        donl(hulu[1]["href"], "pinterest.jpg")
        await e.delete()
        await e.client.send_file(
            e.chat_id, "pinterest.mp4", thumb="pinterest.jpg", caption=f"Pin:- {m}"
        )
        os.remove("pinterest.mp4")
        os.remove("pinterest.jpg")
    else:
        await e.delete()
        await e.client.send_file(e.chat_id, hulu[0]["href"], caption=f"Pin:- {m}")

@ultroid_cmd(pattern="logo ?(.*)")
async def logo_gen(event):
    xx = await eor(event, get_string("com_1"))
    name = event.pattern_match.group(1)
    if not name:
        await eod(xx, "`Give a name too!`")
    bg_, font_ = "", ""
    if event.reply_to_msg_id:
        temp = await event.get_reply_message()
        if temp.media:
            if hasattr(temp.media, "document"):
                if "font" in temp.file.mime_type:
                    font_ = await temp.download_media()
                elif (".ttf" in temp.file.name) or (".otf" in temp.file.name):
                    font_ = await temp.download_media()
            elif "pic" in mediainfo(temp.media):
                bg_ = await temp.download_media()
    else:
        pics = []
        async for i in event.client.iter_messages(
            "@UltroidLogos", filter=InputMessagesFilterPhotos
        ):
            pics.append(i)
        id_ = random.choice(pics)
        bg_ = await id_.download_media()
        fpath_ = glob.glob("resources/fonts/*")
        font_ = random.choice(fpath_)
    if not bg_:
        pics = []
        async for i in event.client.iter_messages(
            "@UltroidLogos", filter=InputMessagesFilterPhotos
        ):
            pics.append(i)
        id_ = random.choice(pics)
        bg_ = await id_.download_media()
    if not font_:
        fpath_ = glob.glob("resources/fonts/*")
        font_ = random.choice(fpath_)
    if len(name) <= 8:
        fnt_size = 150
        strke = 10
    elif len(name) >= 9:
        fnt_size = 50
        strke = 5
    else:
        fnt_size = 130
        strke = 20
    img = Image.open(bg_)
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype(font_, fnt_size)
    w, h = draw.textsize(name, font=font)
    h += int(h * 0.21)
    image_width, image_height = img.size
    draw.text(
        ((image_width - w) / 2, (image_height - h) / 2),
        name,
        font=font,
        fill=(255, 255, 255),
    )
    x = (image_width - w) / 2
    y = (image_height - h) / 2
    draw.text(
        (x, y), name, font=font, fill="white", stroke_width=strke, stroke_fill="black"
    )
    flnme = f"ultd.png"
    img.save(flnme, "png")
    await xx.edit("`Done!`")
    if os.path.exists(flnme):
        await event.client.send_file(
            event.chat_id,
            file=flnme,
            caption=f"Logo by [{OWNER_NAME}](tg://user?id={OWNER_ID})",
            force_document=True,
        )
        os.remove(flnme)
        await xx.delete()
    if os.path.exists(bg_):
        os.remove(bg_)
    if os.path.exists(font_):
        if not font_.startswith("resources/fonts"):
            os.remove(font_)