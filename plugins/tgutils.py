# inspired from bin.py which was made by @danish_00
# written by @senku_ishigamiii/@Uzumaki_Naruto_XD

"""
✘ Commands Available -

• `{i}limited`
   Check you are limited or not !

•`{i}tscan`
   Get The channel/groups user is in.\nReply to Message or give username/id"

• `{i}totalmsgs`
    Returns your total msg count in current chat
    
• `{i}totalmsgs [username]/<reply>`
    Returns total msg count of user in current chat

• `{i}type <msg>`
    Edits the Message and shows like someone is typing

• `{i}sg <reply to a user><username/id>`
    Get His Name History of the replied user.

• `{i}tr <dest lang code> <(reply to) a message>`
    Get translated message.

• `{i}wspr <username>`
    Send secret message..


"""
import asyncio
from telethon.utils import get_display_name
from telethon.utils import get_display_name
from google_trans_new import google_translator
from telethon.tl.types import ChannelParticipantAdmin, ChannelParticipantsBots
from telethon import events
from telethon.errors.rpcerrorlist import YouBlockedUserError

from . import *


@ultroid_cmd(pattern="limited$")
async def demn(ult):
    chat = "@SpamBot"
    msg = await eor(ult, "Checking If You Are Limited...")
    async with ult.client.conversation(chat) as conv:
        try:
            response = conv.wait_event(
                events.NewMessage(incoming=True, from_users=178220800)
            )
            await conv.send_message("/start")
            response = await response
            await ult.client.send_read_acknowledge(chat)
        except YouBlockedUserError:
            await msg.edit("Boss! Please Unblock @SpamBot ")
            return
        await msg.edit(f"~ {response.message.message}")

@ultroid_cmd(pattern="tscan ?(.*)")
async def tscan(e):
    mat = e.pattern_match.group(1)
    if e.is_reply and not mat:
        re = await e.get_reply_message()
        mat = re.sender_id
    if not mat:
        return await eor(e, "Give me Something to look for.")
    chat = "@tgscanrobot"
    async with e.client.conversation(chat) as bot:
        await bot.send_message(str(mat))
        a = await bot.get_response()
    await eor(e, a.message)
    await e.client.send_read_acknowledge(chat)

@ultroid_cmd(pattern="totalmsgs ?(.*)")
async def _(e):
    match = e.pattern_match.group(1)
    if match:
        user = match
    elif e.is_reply:
        user = (await e.get_reply_message()).sender_id
    else:
        user = "me"
    a = await e.client.get_messages(e.chat_id, 0, from_user=user)
    user = await e.client.get_entity(user)
    await eor(e, f"Total msgs of `{get_display_name(user)}` here = {a.total}")

@ultroid_cmd(pattern="type ?(.*)", fullsudo=True)
async def _(event):
    input_str = event.pattern_match.group(1)
    if not input_str:
        return await eor(event, "Give me something to type !")
    shiiinabot = "\u2060" * 602
    okla = await eor(event, shiiinabot)
    typing_symbol = "|"
    previous_text = ""
    await okla.edit(typing_symbol)
    await asyncio.sleep(0.4)
    for character in input_str:
        previous_text = previous_text + "" + character
        typing_text = previous_text + "" + typing_symbol
        await okla.edit(typing_text)
        await asyncio.sleep(0.4)
        await okla.edit(previous_text)
        await asyncio.sleep(0.4)

@ultroid_cmd(
    pattern="sg ?(.*)",
)
async def lastname(steal):
    mat = steal.pattern_match.group(1)
    if not steal.is_reply and not mat:
        return await eor(steal, "`Use this command with reply or give Username/id...`")
    if mat:
        user_id = await get_user_id(mat)
    message = await steal.get_reply_message()
    if message:
        user_id = message.sender.id
    chat = "@SangMataInfo_bot"
    id = f"/search_id {user_id}"
    lol = await eor(steal, get_string("com_1"))
    try:
        async with steal.client.conversation(chat) as conv:
            try:
                msg = await conv.send_message(id)
                response = await conv.get_response()
                respond = await conv.get_response()
                responds = await conv.get_response()
            except YouBlockedUserError:
                return await lol.edit("Please unblock @sangmatainfo_bot and try again")
            if (
                (response and response.text == "No records found")
                or (respond and respond.text == "No records found")
                or (responds and responds.text == "No records found")
            ):
                await lol.edit("No records found for this user")
                await steal.client.delete_messages(conv.chat_id, [msg.id, response.id])
            elif response.text.startswith("🔗"):
                await lol.edit(respond.message)
                await lol.reply(responds.message)
            elif respond.text.startswith("🔗"):
                await lol.edit(response.message)
                await lol.reply(responds.message)
            else:
                await lol.edit(respond.message)
                await lol.reply(response.message)
            await steal.client.delete_messages(
                conv.chat_id,
                [msg.id, responds.id, respond.id, response.id],
            )
    except AsyncTimeout:
        await lol.edit("Error: @SangMataInfo_bot is not responding!.")

@ultroid_cmd(pattern="tr", type=["official", "manager"])
async def _(event):
    if len(event.text) > 3 and event.text[3] != " ":
        return
    input = event.text[4:6]
    txt = event.text[7:]
    if txt:
        text = txt
        lan = input or "en"
    elif event.is_reply:
        previous_message = await event.get_reply_message()
        text = previous_message.message
        lan = input or "en"
    else:
        return await eor(
            event, f"`{HNDLR}tr LanguageCode` as reply to a message", time=5
        )
    translator = google_translator()
    try:
        tt = translator.translate(text, lang_tgt=lan)
        fr = translator.detect(text)
        output_str = f"**TRANSLATED** from {fr} to {lan}\n{tt}"
        await eor(event, output_str)
    except Exception as exc:
        await eor(event, str(exc), time=5)
