# Ultroid - UserBot
# Copyright (C) 2021 TeamUltroid
#
# This file is a part of < https://github.com/TeamUltroid/Ultroid/ >
# PLease read the GNU Affero General Public License in
# <https://www.github.com/TeamUltroid/Ultroid/blob/main/LICENSE/>.
"""
✘ Commands Available -

• `{i}webshot <url>`
    Get a screenshot of the webpage.

• `{i}tss <Twitter link or ID>`

E.g: https://twitter.com/jack/status/969234275420655616
Or ID:
E.g: 969234275420655616

    Gives Screenshot of Tweet.
"""
import os

from htmlwebshot import WebShot
from telethon.errors.rpcerrorlist import YouBlockedUserError
from telethon.tl.functions.account import UpdateNotifySettingsRequest
from . import *
from . import eor, get_string, is_url_ok, ultroid_cmd


@ultroid_cmd(pattern="webshot ?(.*)")
async def webss(event):
    xx = await eor(event, get_string("com_1"))
    xurl = event.pattern_match.group(1)
    if not xurl:
        return await eor(xx, get_string("wbs_1"), time=5)
    elif not is_url_ok(xurl):
        return await eor(xx, get_string("wbs_2"), time=5)
    shot = WebShot(quality=88, flags=["--enable-javascript", "--no-stop-slow-scripts"])
    pic = await shot.create_pic_async(url=xurl)
    await xx.reply(
        get_string("wbs_3").format(xurl),
        file=pic,
        link_preview=False,
        force_document=True,
    )
    os.remove(pic)
    await xx.delete()

@ultroid_cmd(pattern="tss(.*)$")
async def demn(ult):
    input = ult.pattern_match.group(1)
    chat = "@TweetShotBot"
    await ult.edit("Please Wait")
    async with ult.client.conversation(chat) as conv:
        try:
            response = conv.wait_event(events.NewMessage(incoming=True, from_users=1073646153))
            await ult.client.send_message(chat, f"{input}")
            response = await response
        except YouBlockedUserError:
            await ult.reply("Boss! Please Unblock @TweetShotBot")    
            return
        x = response.text
        z = x.split("\n")[(len(x.split("\n")))-1]
        await ult.reply(input, file=response.media)

HELP.update({f"{__name__.split('.')[1]}": f"{__doc__.format(i=HNDLR)}"})
